const {queryLauncher} = require('../sql.js');

// Movies:

// - Get all movies

const getAllMovies = () =>{
    const sql = 'SELECT * FROM Movies';
    return queryLauncher(sql);
}

// - Get the movie with given ID

const getMovieWithID = (id) =>{
    const sql = "SELECT * FROM Movies where `Rank` ="+id;
    return queryLauncher(sql);
}

// Add a new movie

const AddNewMovie = (movie) =>{
    const sql = `Insert into Movies (Title,Description,Runtime,Genre,Rating,Metascore,Votes,Gross_Earning_in_Mil,Director,Actor,Year) values("${movie.title}","${movie.description}",${movie.runtime},"${movie.genre}",${movie.rate},${movie.metascore},${movie.vote},${movie.gross},"${movie.dir}","${movie.actor}",${movie.year});`;
    return queryLauncher(sql);
}

// - Update the movie with given ID
const checkMovierank=(id)=> {
    const sql1 = "Select `Rank` from Movies where `Rank` ="+id +"having COUNT(`Rank`)>0;";
    return queryLauncher(sql1);
  }
const UpdateMovie = (id,object) =>{
    const sql = "Update Movies set"+` ${object.column}`+"="+`${object.value}` + "where `Rank` ="+`${id};`;
    console.log(sql)
    return queryLauncher(sql);
}

const deleteMovies = (id) => {
    const sql = 'DELETE FROM Movies WHERE `Rank` = '+id;
    return queryLauncher(sql);
  }


module.exports = {getAllMovies, getMovieWithID, AddNewMovie,checkMovierank, UpdateMovie, deleteMovies}