const { queryLauncher } = require('../sql.js');

// Directors:

// - Get all directors

const getAllDirectors = () =>{
    const sql = 'SELECT * FROM Directors;';
    return queryLauncher(sql);
}

// - Get the director with given ID

const getDirectorwithID = (id) =>{
    const sql = `SELECT * FROM Directors WHERE id=${id};`;
    return queryLauncher(sql);
}

// Add a new director

const AddNewDirector = (name) =>{
    const sql= `INSERT INTO Directors (Director) values('${name}')`
    return queryLauncher(sql);
}

// Update the director with given ID
const checkDirectorId=(id)=> {
    const sql1 = `Select id from Directors where id =${id} having COUNT(id)>0;`;
    return queryLauncher(sql1);
  }
const UpdateDirector = (id,name) =>{
    const sql = `UPDATE Directors SET Director='${name}' WHERE id=${id};`;
    return queryLauncher(sql);
}

// Delete the director with given ID

const DeleteDirector = (id) =>{
    const sql = `DELETE FROM Directors where id=${id};`;
    return queryLauncher(sql);
}


module.exports = {getAllDirectors, getDirectorwithID, AddNewDirector, checkDirectorId, UpdateDirector, DeleteDirector};