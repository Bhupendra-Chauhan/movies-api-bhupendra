const fs = require("fs");
const connection = require("./sql.js").connection;

const data = fs.readFileSync("movies_3.json");

const movies = JSON.parse(data);

const ReplaceNA = (obj) => {
  for (let value of Object.keys(obj)) {
    if (obj[value] === "NA") {
      obj[value] = 0;
    }
  }
  return obj;
};

const insertDatainMovies = (entry) => {
  const sql = `INSERT INTO Movies values (${entry.Rank}, "${entry.Title}", "${entry.Description}", ${entry.Runtime}, '${entry.Genre}', ${entry.Rating}, ${entry.Metascore}, ${entry.Votes}, ${entry.Gross_Earning_in_Mil}, "${entry.Director}", "${entry.Actor}", ${entry.Year});`;
  connection.query(sql, (err, result) => {
    if (err) throw err;
    console.log("Result", JSON.stringify(result));
  });
};

const insertDatainDirectors = (id, entry) => {
  const sql = `INSERT INTO Directors values (${id}, '${entry.Director}');`;
  connection.query(sql, (err, result) => {
    if (err) throw err;
    console.log("Result", JSON.stringify(result));
  });
};

connection.connect((err) => {
  if (err) throw err;
  console.log("Database Connected!!!");
  for (let movie of movies) {
    if (Object.values(movie).includes("NA")) {
      movie = ReplaceNA(movie);
      insertDatainMovies(movie);
    } else {
      insertDatainMovies(movie);
    }
  }
});

connection.connect(() => {
  console.log("Database connected!!!!");
  const sql = "CREATE TABLE Directors(id INT, Director VARCHAR(30));";
  connection.query(sql, (err, result) => {
    if (err) throw err;
    console.log("Result", JSON.stringify(result));
  });
});

connection.connect(() => {
  console.log("Database Connected!!!");
  let count = 0;
  const director = {};
  for (let movie of movies) {
    if (!director[movie.movies]) {
      count += 1;
      insertDatainDirectors(count, movie);
      director[movie.Director] = 1;
    }
  }
});
