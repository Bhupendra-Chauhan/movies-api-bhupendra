var mysql      = require('mysql');
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'Bhup@123',
  database : 'REST_API'
});
 
function queryLauncher(sql) {
  return new Promise((resolve, reject) => {
    connection.query(sql, (error, result) => {
      if (error) {
        return reject(error);
      }
      console.log(JSON.stringify(result));
      return resolve(result);
    });
  });
}
module.exports = { connection,queryLauncher };
 